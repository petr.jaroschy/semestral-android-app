package cz.cuni.mff.semestralvolumeone.Tasks;

import android.os.AsyncTask;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.javanet.NetHttpTransport;

import java.io.IOException;

import cz.cuni.mff.semestralvolumeone.OnTaskDoneListener;

public class RetrieveCurrentPlayerCountJsonTask extends AsyncTask<Long, Void, String> {
    private OnTaskDoneListener onTaskDoneListener;
    private static final String url = "https://api.steampowered.com/ISteamUserStats/GetNumberOfCurrentPlayers/v0001/?appid=";
    public RetrieveCurrentPlayerCountJsonTask(OnTaskDoneListener onTaskDoneListener) {
        this.onTaskDoneListener = onTaskDoneListener;
    }

    /**
     * @param longs no parameters are taken by this task.
     * @return json of all apps on steam.
     */
    @Override
    protected String doInBackground(Long... longs) {

        HttpRequestFactory requestFactory = new NetHttpTransport().createRequestFactory();
        String rawResponse = null;
        try {
            HttpRequest request = requestFactory.buildGetRequest(new GenericUrl(url + longs[0]));
            rawResponse = request.execute().parseAsString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rawResponse;
    }

    @Override
    protected void onPostExecute(String s) {
        onTaskDoneListener.onTaskDone(s);
    }
}
