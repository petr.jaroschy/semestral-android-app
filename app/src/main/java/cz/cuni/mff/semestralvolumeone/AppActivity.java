package cz.cuni.mff.semestralvolumeone;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.google.gson.Gson;

import cz.cuni.mff.semestralvolumeone.Adapters.NewsListAdapter;
import cz.cuni.mff.semestralvolumeone.Entities.NewsEntity;
import cz.cuni.mff.semestralvolumeone.Entities.PlayerCountEntity;
import cz.cuni.mff.semestralvolumeone.Tasks.RetrieveCurrentPlayerCountJsonTask;
import cz.cuni.mff.semestralvolumeone.Tasks.RetrieveNewsForAppJsonTask;

/**
 * Activity for showing list of news for given app.
 */
public class AppActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        Integer appid = bundle.getInt("appid");

        String appname = bundle.getString("appname");
        setTitle(appname);

        final TextView playercountlabel = findViewById(R.id.PlayerCountCountLabel);

        new RetrieveCurrentPlayerCountJsonTask(responseData -> {
            Gson gson = new Gson();
            PlayerCountEntity playerCountEntity = gson.fromJson(responseData, PlayerCountEntity.class);
            if (playerCountEntity == null || playerCountEntity.response == null) {
                playercountlabel.setText("unknown");
                return;
            }
            playercountlabel.setText(String.valueOf(playerCountEntity.response.player_count));
        }).execute(appid.longValue());

        final RecyclerView recyclerView = findViewById(R.id.news_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        new RetrieveNewsForAppJsonTask(responseData -> {
            Gson gson = new Gson();
            NewsEntity newsEntity = gson.fromJson(responseData, NewsEntity.class);
            if (newsEntity == null || newsEntity.appnews.newsitems == null) {
                return;
            }
            recyclerView.setAdapter(new NewsListAdapter(newsEntity.appnews.newsitems));
        }).execute(appid.longValue());


    }
}

