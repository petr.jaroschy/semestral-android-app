package cz.cuni.mff.semestralvolumeone.Tasks;

import android.os.AsyncTask;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.javanet.NetHttpTransport;

import java.io.IOException;

import cz.cuni.mff.semestralvolumeone.OnTaskDoneListener;

/**
 * Task which retrieves json of all steam apps and their ids
 */
public class RetrieveAppsJsonTask extends AsyncTask<Void, Void, String> {
    private OnTaskDoneListener onTaskDoneListener;
    private static final String url = "https://api.steampowered.com/ISteamApps/GetAppList/v0002/";
    public RetrieveAppsJsonTask(OnTaskDoneListener onTaskDoneListener) {
        this.onTaskDoneListener = onTaskDoneListener;
    }

    /**
     * @param voids no parameters are taken by this task.
     * @return json of all apps on steam.
     */
    @Override
    protected String doInBackground(Void... voids) {

        HttpRequestFactory requestFactory = new NetHttpTransport().createRequestFactory();
        String rawResponse = null;
        try {
            HttpRequest request = requestFactory.buildGetRequest(new GenericUrl(url));
            rawResponse = request.execute().parseAsString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rawResponse;
    }

    @Override
    protected void onPostExecute(String s) {
        onTaskDoneListener.onTaskDone(s);
    }
}
