package cz.cuni.mff.semestralvolumeone.Tasks;

import android.os.AsyncTask;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.javanet.NetHttpTransport;

import java.io.IOException;

import cz.cuni.mff.semestralvolumeone.OnTaskDoneListener;

/**
 * Task for retrieving json of news for given app
 */
public final class RetrieveNewsForAppJsonTask extends AsyncTask<Long, Void, String> {
    private OnTaskDoneListener onTaskDoneListener;

    public RetrieveNewsForAppJsonTask(OnTaskDoneListener onTaskDoneListener) {
        this.onTaskDoneListener = onTaskDoneListener;
    }

    /**
     * @param params First long is always app id, second is count of news, third is date up to which you want news.
     *               Default count is 20.
     *               Default end date is max value of long.
     * @return json of news for given app on steam.
     */
    @Override
    protected String doInBackground(Long... params) {
        HttpRequestFactory requestFactory = new NetHttpTransport().createRequestFactory();
        Long appId = params[0];
        Long count, enddate;
        if (params.length > 1) {
            count = params[1];
        } else {
            count = 20L;
        }
        if (params.length > 2) {
            enddate = params[2];
        } else {
            enddate = Long.MAX_VALUE;
        }
        String rawResponse = null;
        try {
            // FIXME: I think UriBuilder or something else would be much better than to concatenate it by yourself;;; maybe next time
            HttpRequest request = requestFactory.buildGetRequest(new GenericUrl("https://api.steampowered.com/ISteamNews/GetNewsForApp/v0002/?appid=" + appId + "&count=" + count + "&enddate=" + enddate));
            rawResponse = request.execute().parseAsString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rawResponse;
    }

    @Override
    protected void onPostExecute(String s) {
        onTaskDoneListener.onTaskDone(s);
    }
}
