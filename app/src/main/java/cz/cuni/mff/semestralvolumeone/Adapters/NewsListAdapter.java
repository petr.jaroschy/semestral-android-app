package cz.cuni.mff.semestralvolumeone.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cz.cuni.mff.semestralvolumeone.Entities.NewsEntity;
import cz.cuni.mff.semestralvolumeone.NewsDetailActivity;
import cz.cuni.mff.semestralvolumeone.R;

/**
 * Class representing an adapter for showing list of news for app.
 */
public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.NewsListViewHolder> {
    private NewsEntity.AppNews.News[] mDataset;

    static class NewsListViewHolder extends RecyclerView.ViewHolder {
        TextView view;

        NewsListViewHolder(View v) {
            super(v);
            view = v.findViewById(R.id.app_name);
        }
    }

    public NewsListAdapter(NewsEntity.AppNews.News[] myDataset) {
        mDataset = myDataset;
    }

    @NonNull
    @Override
    public NewsListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.main_list_item, parent, false);
        return new NewsListViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsListViewHolder holder, int position) {
        holder.view.setText(mDataset[position].title);
        holder.view.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("title", mDataset[position].title);
            bundle.putString("content", mDataset[position].contents);
            Context context = holder.view.getContext();
            Intent intent = new Intent(context, NewsDetailActivity.class);
            intent.putExtras(bundle);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
