package cz.cuni.mff.semestralvolumeone.Entities;

import java.io.Serializable;

public class PlayerCountEntity implements Serializable {
    public Response response;

    public class Response {
        public int player_count;
        public int result;
    }
}
