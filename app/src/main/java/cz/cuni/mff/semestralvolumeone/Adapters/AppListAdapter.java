package cz.cuni.mff.semestralvolumeone.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import cz.cuni.mff.semestralvolumeone.AppActivity;
import cz.cuni.mff.semestralvolumeone.Entities.App;
import cz.cuni.mff.semestralvolumeone.R;

/**
 * Adapter for viewing the list of apps.
 */
public class AppListAdapter extends RecyclerView.Adapter<AppListAdapter.AppListViewHolder> {
    private ArrayList<App> data;

    static class AppListViewHolder extends RecyclerView.ViewHolder {
        TextView view;

        AppListViewHolder(View v) {
            super(v);
            view = v.findViewById(R.id.app_name);
        }
    }

    public AppListAdapter(ArrayList<App> mydata) {
        data = mydata;
    }

    @NonNull
    @Override
    public AppListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.main_list_item, parent, false);
        return new AppListViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull AppListViewHolder holder, int position) {
        holder.view.setText(data.get(position).name);
        holder.view.setOnClickListener(v1 -> {
            Context context = v1.getContext();
            Intent intent = new Intent(context, AppActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("appid", data.get(position).appid);
            bundle.putString("appname", data.get(position).name);
            intent.putExtras(bundle);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
